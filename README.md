# Infrastructure

## Projets hébergés

### giletsjaunes-coordination.fr

**URL :** giletsjaunes-coordination.fr  
**Vhost site principal :** portailgj.conf  
**Site principal :** /home/sites/portailgj/prod  
**Vhost outils Thomas :** coordgj.conf  
**Outils Thomas :** /home/sites/coordgj/portail-prod  

Le site principal est un Wordpress.  
Certains éléments du site, qui viennent de l'ancienne version de gj-c créée par Thomas, ne sont pas intégrés dans wordpress.  
Ils sont sur un framework custom créé par Thomas, dans "coordgj", et la connexion est faite entre les 2 grâce à un script créé par Thomas dans le template wordpress : reverseproxy.php
Des copies de test existent pour chacunes des 2 parties du projet (wordpress et framework Thomas).
À terme, le plan est de tout passer sous wordpress pour abandonner la partie sur le framework de Thomas, puisque nous ne savons pas l'utiliser...

### Nextcloud ADA

**URL :** adacloud.giletsjaunes-coordination.fr  
**Folder :** /home/sites/adacloud  

Un serveur Collabora est lancé via Docker et connecté au Nextcloud.  
Le vhost collabora.giletsjaunes-coordination.fr.conf lui est nécessaire.

### culture-ric.fr

Culture-RIC est hébergé sur le serveur.

### ipgj.fr

**URL :** ipgj.fr  
**Folder :** /home/sites/ipgj  

Le site IPGJ très sommaire que Thomas avait fait à l'époque (sous son framework si je ne m'abuse).
On peut imaginer en faire quelque chose je ne sais pas... 

### Le vrai débat

**URL :** le-vrai-debat.fr  
**Folder :** /home/sites/le-vrai-debat  

Le site du Vrai débat, c'est un Wordpress assez sommaire.

### Messager

**URL :** messager.gj-c.fr  
**Folder :** /home/sites/messager  

Fait par Mira et un ami, je cite Mira pour l'explication :  
Le but c'est que par mail ou sur facebook par exemple, au lieu que ça fasse :
j'écris mon message sur facebook >>> les autres le recoivent  
Ça fasse :  
j'écris mon message au messager >>> j'envoie le messager sur facebook >>> les gens vont sur messager pour voir le message avec un mot de passe (ou pas)  
Comme ça, facebook ne peut pas chopper ce qui est écrit.


## Gestion des domaines

**Nous avons la gestion de :**
- giletjaune-france.fr
- gj-c.fr
- gj-f.fr

**On peut demander des ajustements techniques à Macduf sur :**
- giletsjaunes-coordination.fr

## Liste des vhosts

- adacloud.giletsjaunes-coordination.fr
- collabora.giletsjaunes-coordination.fr.conf
- coordgj.conf
- coordgj_csstest.conf
- coordgj-portail.conf
- coordgj-portail_test.conf
- coordgj_test.conf
- culture-ric.gj-c.fr.conf
- gj-f.conf
- ipgj.conf
- levraidebat.conf
- levraidebat_test.conf
- messager.conf
- phpbb.conf
- portailgj.conf
- portailgj_test.conf
- votez-pour-vous.conf